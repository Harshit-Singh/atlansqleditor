# Sql Editor

## [Demo](https://atlansql.herokuapp.com)

For Page load time - please check console (F-12) in Developer tools to see the DOM load time.

It is calculated using the <code>performance.now()</code> function
1. The start time is taken at the start of <code>\<head></code> tag.
2. The end time is taken at <code>mounted().</code>
3. The difference gives the DOM load time.

To get more accurate page load time please check using the Developer Tools -> Network

## Instructions for use
1. Choose a Database from dropdown from View Database - Orders in this case.
2. This will display the Sql Editor Interface and a separate column with dropdown option of the selected database to choose a table from the database.
3. On selecting a particular table an addition list of columns can be viewed for the table on the right of the editor.
4. The complete table is fetched and displayed on the bottom. This area can be used to display various SQL query results.
